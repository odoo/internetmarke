{
    'name': 'Deutsche Post Internetmarke Shipping Integration',
    'category': 'Website/Shipping Logistics',
    'summary': 'Integrate Deutsche Post shipping functionality directly within Odoo ERP applications to deliver increased logistical efficiencies.',
    'website': '',
    'version': '0.2',
    'description':"""
    """,
    'author': 'Harald Welte',
    'depends': ['odoo_shipping_service_apps','customs_description'],
    'data': [
        'views/dp_delivery_carrier.xml',
         'views/res_config.xml',
         'data/data.xml',
         'data/delivery_demo.xml',
         'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': True,
    'external_dependencies': {
        'python': ['inema']
    },
}
