from openerp import fields, models, api
import logging

_logger = logging.getLogger(__name__)

class website_config_settings(models.Model):
	_inherit = 'website.config.settings'
	_name = 'website.dp.config.settings'

        dp_partner_id = fields.Char('Partner ID', required=1)
        dp_key = fields.Char('Key', required=1)
        dp_key_phase = fields.Char('Key Phase', required=1)
        dp_portokasse_user = fields.Char('Portokasse User', required=1)
        dp_portokasse_passwd = fields.Char('Portokasse Password', required=1)
        dp_wpi_ekp = fields.Char('EKP Number', required=1)
        dp_wpi_sandbox = fields.Boolean('Use WPI Sandbox environment')

	@api.model
	def get_default_dp_values(self, fields):
		ir_values = self.env['ir.values']
		dp_config_values_list_tuples = ir_values.get_defaults('delivery.carrier')
		dp_config_values = {}
		for item in dp_config_values_list_tuples:
			dp_config_values.update({item[1]:item[2]})
		return dp_config_values

	@api.one
	def set_dp_values(self):
		ir_values = self.env['ir.values']
		for config in self:
			ir_values.set_default('delivery.carrier', 'dp_partner_id', config.dp_partner_id or '')
			ir_values.set_default('delivery.carrier', 'dp_key', config.dp_key or '')
			ir_values.set_default('delivery.carrier', 'dp_key_phase', config.dp_key_phase or '')
			ir_values.set_default('delivery.carrier', 'dp_portokasse_user', config.dp_portokasse_user or '')
			ir_values.set_default('delivery.carrier', 'dp_portokasse_passwd', config.dp_portokasse_passwd or '')
			ir_values.set_default('delivery.carrier', 'dp_wpi_ekp', config.dp_wpi_ekp or '')
			ir_values.set_default('delivery.carrier', 'dp_wpi_sandbox', config.dp_wpi_sandbox or False)
		return True
