from openerp import api, fields, models

# extnd delivery.carrier with DP
class SMCShippingDp(models.Model):
    _inherit="delivery.carrier"
    delivery_type = fields.Selection(selection_add=[('dp','Deutsche Post')])
    dp_service_class = fields.Many2one(comodel_name = 'delivery.carrier.dp.class', string = 'DP Service Class')

# New model for DP Classes (regular mail, registered mail, ...)
class SMCShippingDpClass(models.Model):
    _name = "delivery.carrier.dp.class"
    name = fields.Char(string="Name", required=1)
    is_wpi = fields.Boolean("Warenpost International")
    # list of services in this class for national delivery
    services_natl = fields.Many2many(comodel_name = 'delivery.carrier.dp.service',
                                     relation = 'dp_class_natl_services_rel')
    # list of services in this class for EU delivery
    services_eu = fields.Many2many(comodel_name = 'delivery.carrier.dp.service',
                                   relation = 'dp_class_eu_services_rel')
    # list of services in this class for international delivery
    services_intl = fields.Many2many(comodel_name = 'delivery.carrier.dp.service',
                                     relation = 'dp_class_intl_services_rel')

# New model for DP Products/Services (from ProdWS)
class SMCShippingDpService(models.Model):
    _name = "delivery.carrier.dp.service"
    name = fields.Char(string="Name", required=1)
    short_name = fields.Char(string="Short Name")
    code = fields.Integer("Product Code", required=1)
    prodws_id = fields.Integer("ProdWS ID")
    cost_price = fields.Float("Price", required=1)
    weight = fields.Float("Allowed Weight", required=1)
    height = fields.Integer("Height")
    width = fields.Integer("Width")
    length = fields.Integer("Length")
    international = fields.Boolean("International")
    eu = fields.Boolean("EU")

# extend stock.picking with fields for Shipment and Voucher ID
class SMCStockPickingDp(models.Model):
    _inherit = "stock.picking"
    dp_shipment_number = fields.Char(string="DP Shipment ID")
    dp_voucher_number = fields.Char(string="DP Voucher ID")
